
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

*/
window.addEventListener("load", function(){
  var inputName = document.getElementById('name')
  var inputAge = document.getElementById('age');
  var inputGender = document.getElementById('gender');
  var stringifyButton = document.getElementById('stringify');
  var parseButton = document.getElementById('parse');
  var convertData = document.getElementById('parcedata');

  stringifyButton.addEventListener("click", function(){

    let data = {
      name: inputName.value,
      age: inputAge.value,
      gender: inputGender.value
    }

    let stringifyData = JSON.stringify( data );
    console.log( stringifyData );
  })

  parseButton.addEventListener('click', function(){
    
    let data = convertData.value;
    
    let parseData = JSON.parse( data );
    console.log( parseData );
  })


})



