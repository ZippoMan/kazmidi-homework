/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
// function getCompanys(){
//   return fetch ("https://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2")
//     .then ( response => response.json() )
// }

async function getCompanies(){
  const response = await fetch ("https://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2")
  const data = await response.json()
  let table = document.createElement('table');
  table.innerHTML = '<tbody></tbody>';
  table.setAttribute('border', 1);
  table.setAttribute('bgcolor', 'white');

  data.forEach(elem => {
                                                                                                                                                                          
    const tableBody = table.querySelector('tbody');
    const tr = document.createElement('tr');
    tr.innerHTML =  `
    <td>${elem.company}</td>
    <td>${elem.balance}</td>
    <td>
      <div class='date'>
        <button class='registeredDate'>Показать дату регистрации</button>
      </div>
    </td>
    <td>
      <div class='adress'>
       <button class='adress'>Показать адрес</button>
      </div>
    </td>`

    const btnRegisteredDate = tr.querySelector('.registeredDate');
    const divDate = tr.querySelector('.date');
    btnRegisteredDate.addEventListener('click', function() {
      divDate.innerHTML = elem.registered
    })

    const adress = `${elem.address.country}, ${elem.address.city}, ${elem.address.street}, ${elem.address.house}`;
    const btnGetAdress = tr.querySelector('.adress');
    const divAdress = tr.querySelector('.adress');
    btnGetAdress.addEventListener('click', function() {
      divAdress.innerHTML = adress
      })
    tableBody.appendChild(tr);
    });


  document.body.innerHTML = '';
  document.body.appendChild(table)
}

getCompanies()

