var selectedButtons = document.querySelectorAll ('.showButton');
var tabs = document.querySelectorAll('.tab');

selectedButtons[0].onclick = function(event) {
  var firstTab = document.querySelectorAll('.tab');
  firstTab[0].classList.add('active')
}
selectedButtons[1].onclick = function(event) {
  var secondTab = document.querySelectorAll('.tab');
  secondTab[1].classList.add('active')
  
}
selectedButtons[2].onclick = function(event) {
  var thirdTab = document.querySelectorAll('.tab');
  thirdTab[2].classList.add('active')
  
}

function hideAllTabs (){
  tabs.forEach(function(tab){
    tab.classList.remove('active')
  })
    
}



  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
