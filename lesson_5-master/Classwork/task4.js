/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)
*/

var encryptInput = document.getElementById('encryptInput');
var encryptButton = document.getElementById('encryptButton');
var decryptButton = document.getElementById('decryptButton');
var resultEncryption = document.getElementById('resultEncryption');
var resultDecryption = document.getElementById('resultDecryption');


encryptButton.addEventListener("click", function(){
  let word = encryptInput.value;
  encryptInput.value = '';
  let map = Array.prototype.map;
  let charCode = map.call(word, function( code ) { 
    return code.charCodeAt(0);
  });
  
  let encrypt = charCode.map(function(num) {
    return num + 20;
  });

  let encryptData = String.fromCharCode.apply(String, encrypt);

  resultEncryption.innerText = encryptData; 
});

decryptButton.addEventListener("click", function(){
  let word = decryptInput.value;
  decryptInput.value = '';
  let map = Array.prototype.map;
  let charCode = map.call(word, function( code ) { 
    return code.charCodeAt(0); 
  });
  
  let decrypt = charCode.map(function(num) {
    return num - 20;
  });

  let decryptData = String.fromCharCode.apply(String, decrypt);
  console.log(decrypt)
  resultDecryption.innerText = decryptData; 
})