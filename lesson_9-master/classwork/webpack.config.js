const path = require('path');

module.exports = {
  entry: './src/task1.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  }
};