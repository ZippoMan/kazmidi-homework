/*
  localStorage
  window.localStorage
*/
  // console.log( window.localStorage );
// Запись в ЛС
// localStorage.setItem('myCat', 'Tom');
// localStorage.setItem('back', 'blue');
// // Чтение с ЛС
// var cat = localStorage.getItem("myCat");
// // Удаление с ЛС
// localStorage.removeItem("myCat");
// console.log( cat );
// Если не найдено, вернет Null
// var background = localStorage.getItem("back");
// console.log( window.localStorage );

// let obj = {
//   name: 'Vasya',
//   learning: true
// }
// localStorage.setItem('user', JSON.stringify(obj) );
// console.log( JSON.parse( localStorage.getItem("user") ) );
// // console.log( JSON.parse( localStorage.getItem("JSON") ) );
// //
// if( background !== null){
//   document.body.style.backgroundColor = background;
// }

/*

  Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

  Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/


var button = document.querySelector('.btn');
button.addEventListener('click', function(){
  const randomColor =` #${Math.floor(Math.random()*256).toString(16)}${Math.floor(Math.random()*256).toString(16)}${Math.floor(Math.random()*256).toString(16)} `;
  localStorage.setItem('Color', randomColor)
})

////////////////////////////////////////////////////////

    var formValidate = document.getElementById('MyValidateForm');
    var submitButton = document.getElementById('submit');

    const userData = {
      name: 'Admin',
      login: 'admin@example.com',
      pass: '12345678'
    }

    submitButton.addEventListener('click', function(){
      event.preventDefault();
      var loginIsValid = formValidate.login.value == userData.login && formValidate.pass.value == userData.pass;
      if( loginIsValid ){
        alert(`Hello ${userData.name}`);
        localStorage.setItem('login', formValidate.login.value);
        localStorage.setItem('Password', formValidate.pass.value);
      } else{
        alert(`Invalid username or pass, try again!`)
      }
    })