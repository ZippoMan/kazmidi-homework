var OurSliderImages = ['images/1/1.jpg', 'images/1/2.jpg', 'images/1/3.jpg', 'images/1/4.jpg'];
  var currentPosition = 0;
  var divSlider = document.getElementById('slider');
  var divBigImage = document.getElementById('bigImage');
  var divWrapZoomed = document.getElementById('wrapZoomed');

import mainButtons from './main_buttons.js'
import zoomedButtons from './zoomed_buttons.js'

  window.addEventListener('load', function(){
    divSlider.innerHTML = '<img src="' + OurSliderImages[0] + '"/>';
    mainButtons();
    zoomedButtons();
    divSlider.addEventListener('click', function(){
      divBigImage.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;
      divWrapZoomed.style.display = "block";
    })
  })
  