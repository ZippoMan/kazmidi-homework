var OurSliderImages = ['images/1/1.jpg', 'images/1/2.jpg', 'images/1/3.jpg', 'images/1/4.jpg'];
  var currentPosition = 0;
  var divSlider = document.getElementById('slider');
  var divBigImage = document.getElementById('bigImage');
  var divWrapZoomed = document.getElementById('wrapZoomed');
  var closeButton = document.getElementById('close');
  var zoomedPrevSlide = document.getElementById('ZoomedPrevSlide');
  var zoomedNextSlide = document.getElementById('ZoomedNextSlide');

export default function zoomedButtons(){
    zoomedPrevSlide.addEventListener('click', function(){
      if( currentPosition < 1 ){
        currentPosition = OurSliderImages.length - 1;
        divBigImage.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;
      } else{
        currentPosition--;
        divBigImage.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;  
        divSlider.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;      
      }       
    })

    zoomedNextSlide.addEventListener('click', function(){
      if( currentPosition === OurSliderImages.length - 1 ){
        currentPosition = 0;
        divBigImage.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;
        divSlider.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;
      } else{
      currentPosition++;
      divBigImage.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;
      divSlider.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;
      }

    })
    closeButton.addEventListener('click', function(){
      divWrapZoomed.style.display = "none";
    })
    
  }