var OurSliderImages = ['images/1/1.jpg', 'images/1/2.jpg', 'images/1/3.jpg', 'images/1/4.jpg'];
  var currentPosition = 0;
  var divSlider = document.getElementById('slider');
  var prevButton = document.getElementById('PrevSlide');
  var nextButton = document.getElementById('NextSlide');

export default function mainButtons(){
      prevButton.addEventListener('click', function(){
        if( currentPosition < 1 ){
          currentPosition = OurSliderImages.length - 1;
          divSlider.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;
        } else{
          currentPosition--;
          divSlider.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;        
        }       
      })
      nextButton.addEventListener('click', function(){
        if( currentPosition === OurSliderImages.length - 1 ){
          currentPosition = 0;
          divSlider.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;
        } else{
        currentPosition++;
        divSlider.innerHTML = `<img src="${OurSliderImages[currentPosition]}"/>`;
        }

      })
    }